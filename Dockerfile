FROM python:3

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /app
ADD /config/requirements.txt /app/config/requirements.txt
ADD /config/wait-for-postgres.sh /app/config/wait-for-postgres.sh
WORKDIR /app
RUN ls /app
RUN pip3 install -r /app/config/requirements.txt
ADD jw /app

