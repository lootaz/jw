# Jw project (~20 hours)
## Run
**Runserver**
```
docker-compose up runserver
```
**Execute tests**
```
docker-compose up autotests
```
## Test user for admin
```
login: test
pasword: 0
```
## API Examples

### List of pages
Get list of all pages with url to details

**Request**
```
http://localhost:8000/api/pages/
```
**Response**
```json
{  
   "count":50,
   "next":"http://web/api/pages/?limit=20&offset=20",
   "previous":null,
   "results":[  
      {  
         "id":1,
         "details_url":"/api/pages/1/"
      },
      {  
         "id":2,
         "details_url":"/api/pages/2/"
      },
      
      ...
      
      {  
         "id":20,
         "details_url":"/api/pages/20/"
      }
   ]
}
```

### Page details
Get page details with all content

**Request**
```
http://localhost:8000/api/pages/1
```

**Response**
```json
{
   "id":1,
   "title":"Page 1",
   "content":[
      {
         "id":82,
         "title":"Video 82",
         "counter":0,
         "page":1,
         "url_video":"http://example.com/video/82",
         "url_subtitles":"http://example.com/subtitles/82"
      },
      {
         "id":160,
         "title":"Audio 160",
         "counter":0,
         "page":1,
         "bit_rate":32000
      },
      {
         "id":267,
         "title":"Text 267",
         "counter":0,
         "page":1,
         "text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
      }
   ]
}
```


