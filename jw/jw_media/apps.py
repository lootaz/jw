from django.apps import AppConfig


class JwMediaConfig(AppConfig):
    name = 'jw_media'
