from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Page, VideoContent, AudioContent, TextContent, Content


class PageListSerializer(serializers.ModelSerializer):
    details_url = serializers.SerializerMethodField()

    def get_details_url(self, obj):
        return reverse("api:page-detail", args=[obj.pk])

    class Meta:
        model = Page
        fields = ('id', 'details_url')


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ('id', 'title', 'counter', 'page')


class VideoContentSerializer(ContentSerializer):
    class Meta:
        model = VideoContent
        fields = ContentSerializer.Meta.fields + ('url_video', 'url_subtitles')


class AudioContentSerializer(ContentSerializer):
    class Meta:
        model = AudioContent
        fields = ContentSerializer.Meta.fields + ('bit_rate',)


class TextContentSerializer(ContentSerializer):
    class Meta:
        model = TextContent
        fields = ContentSerializer.Meta.fields + ('text',)


class PageDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ('id', 'title')
