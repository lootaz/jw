from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'pages', views.PageViewSet)
router.register(r'textcontents', views.TextContentViewSet)

app_name = 'jw_media'
urlpatterns = [
    url(r'api/', include(router.urls))
]
