from django.db import models


class TitleEntity(models.Model):
    title = models.CharField(max_length=255)

    class Meta:
        abstract = True


class Page(TitleEntity):
    class Meta:
        db_table = 'jw_page'


class Content(TitleEntity):
    counter = models.IntegerField(default=0)
    page = models.ForeignKey('Page', on_delete=models.CASCADE)

    class Meta:
        db_table = "jw_content"


class VideoContent(Content):
    url_video = models.TextField()
    url_subtitles = models.TextField()

    class Meta:
        db_table = 'jw_video_content'


class AudioContent(Content):
    bit_rate = models.IntegerField()

    class Meta:
        db_table = 'jw_audio_content'


class TextContent(Content):
    text = models.TextField()

    class Meta:
        db_table = 'jw_text_content'
