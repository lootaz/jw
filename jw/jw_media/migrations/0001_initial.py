# Generated by Django 2.1.1 on 2018-09-05 19:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('counter', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'jw_content',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'jw_page',
            },
        ),
        migrations.CreateModel(
            name='AudioContent',
            fields=[
                ('content_ptr',
                 models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True,
                                      primary_key=True, serialize=False, to='jw_media.Content')),
                ('bit_rate', models.IntegerField()),
            ],
            options={
                'db_table': 'jw_audio_content',
            },
            bases=('jw_media.content',),
        ),
        migrations.CreateModel(
            name='TextContent',
            fields=[
                ('content_ptr',
                 models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True,
                                      primary_key=True, serialize=False, to='jw_media.Content')),
                ('text', models.TextField()),
            ],
            options={
                'db_table': 'jw_text_content',
            },
            bases=('jw_media.content',),
        ),
        migrations.CreateModel(
            name='VideoContent',
            fields=[
                ('content_ptr',
                 models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True,
                                      primary_key=True, serialize=False, to='jw_media.Content')),
                ('url_video', models.TextField()),
                ('url_subtitles', models.TextField()),
            ],
            options={
                'db_table': 'jw_video_content',
            },
            bases=('jw_media.content',),
        ),
        migrations.AddField(
            model_name='content',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jw_media.Page'),
        ),
    ]
