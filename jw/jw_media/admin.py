from django.contrib import admin

from .models import Page, VideoContent, AudioContent, TextContent


class PageAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    search_fields = ('title',)


class VideoContentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'counter', 'url_video', 'url_subtitles')
    search_fields = ('title',)


class AudioContentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'counter', 'bit_rate')
    search_fields = ('title',)


class TextContentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'counter', 'text')
    search_fields = ('title',)


admin.site.register(Page, PageAdmin)
admin.site.register(VideoContent, VideoContentAdmin)
admin.site.register(AudioContent, AudioContentAdmin)
admin.site.register(TextContent, TextContentAdmin)
