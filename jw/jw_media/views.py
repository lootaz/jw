from django.db.models import F
from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .serializers import PageListSerializer, TextContentSerializer, PageDetailSerializer, VideoContentSerializer, \
    AudioContentSerializer
from .models import Page, TextContent


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all()
    renderer_classes = (JSONRenderer,)

    action_serializers = {
        'retrieve': PageDetailSerializer,
        'list': PageListSerializer,
    }

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        contents_query = instance.content_set.all()
        content_data = []

        for content in contents_query:
            if hasattr(content, 'videocontent'):
                serializer = VideoContentSerializer(content.videocontent)
            elif hasattr(content, 'audiocontent'):
                serializer = AudioContentSerializer(content.audiocontent)
            elif hasattr(content, 'textcontent'):
                serializer = TextContentSerializer(content.textcontent)

            content_data.append(serializer.data)

            if hasattr(content, 'counter'):
                content.counter = F('counter') + 1
                content.save()

        page_serializer = PageDetailSerializer(instance)
        data = page_serializer.data
        data['content'] = content_data

        return Response(data, status=status.HTTP_200_OK)

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]

        return super(PageViewSet, self).get_serializer_class()


class TextContentViewSet(viewsets.ModelViewSet):
    queryset = TextContent.objects.all()
    serializer_class = TextContentSerializer
    renderer_classes = (JSONRenderer,)
