from django.urls import reverse
from rest_framework.status import HTTP_200_OK
from rest_framework.test import APITestCase

from .models import Page, AudioContent, TextContent


class PageTestCase(APITestCase):
    TEST_PAGES_AMOUNT = 70
    TEST_AUDIO_BIT_RATE = 77777

    def setUp(self):
        Page.objects.all().delete()

    def test_get_pages(self):
        for i in range(self.TEST_PAGES_AMOUNT):
            self.page = Page(title="Test page %i" % i)
            self.page.save()

        response = self.client.get(reverse('api:page-list'))
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data.get('count'), self.TEST_PAGES_AMOUNT)

    def test_get_detail(self):
        page = Page(title="Test page detail")
        page.save()

        audio = AudioContent(page=page, bit_rate=self.TEST_AUDIO_BIT_RATE)
        audio.save()

        response = self.client.get(reverse("api:page-detail", args=[page.pk]))
        self.assertEqual(response.status_code, HTTP_200_OK)

        content = response.data.get('content')
        self.assertEqual(len(content), 1)
        self.assertEqual(content[0].get('bit_rate'), self.TEST_AUDIO_BIT_RATE)

    def test_content_counter(self):
        page = Page(title="Test page detail")
        page.save()

        text = TextContent(page=page,
                           text="Lorem ipsum")
        text.save()

        response = self.client.get(reverse("api:page-detail", args=[page.pk]))
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data.get('content')[0].get('counter'), 0)

        response = self.client.get(reverse("api:page-detail", args=[page.pk]))
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data.get('content')[0].get('counter'), 1)
